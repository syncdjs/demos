# Syncd editor *(Work in Progress)*

- An **interactive** Page Builder which compiles in **JSX**
- Fully compatible with Markdown

[Check the demo for more](https://syncdjs.gitlab.io/demos/)
