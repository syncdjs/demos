import React from "react"
import ReactDom from "react-dom"
import EditorComponent from "./Components/EditArea"

import "./Components/EditArea/style.scss"

const main = document.querySelector("main")
ReactDom.render(<EditorComponent />, main)
