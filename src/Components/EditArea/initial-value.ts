import { BlockJSON, TextJSON, ValueJSON } from 'slate'
import { BlockType as BT } from './helpers/decorate/document/helper'

const type = BT.Line

const toNodes = ([doc]: TemplateStringsArray): BlockJSON[] => {
  // @ts-ignore
  const t = (text: string): TextJSON => ({ object: 'text', leaves: [ { text } ] })
  return doc
    .split("\n")
    .slice(1)
    .map((text): BlockJSON => ({ object: 'block', type, nodes: [ t(text) ] }))
}

const nodes = toNodes`
# Syncd editor *(Work in Progress)*

- An **interactive** Page Builder which compiles in **JSX**
- Fully compatible with Markdown
- Support React components (todo add an example)
- Like Markdown:
   1. Supports nesting blocks
   2. based on indentation
- Supports complex layout:
   <.row>
      <.col-3> by providing control over elements classes, ids, and any html attributes. ![test svg](assets/test.svg "title")
         <.contentless desc="you still can edit empty elements">

      <.col-3>
      Paragraphs are nested in preceding blocks with the same indentation
      <textarea data-x=".col-3"> And so are inline elements

- [Inline elements attributes can also be edited]<.inline> <.fa::lg:cog> <a.cls value="V">Here's a link example</a>
`

const root: BlockJSON = { type: 'root', object: 'block', nodes }
const initalValue: ValueJSON = { document: { nodes: [root] } }

export default initalValue
