import React, { useState } from 'react'
import { Decoration, Editor, Node } from 'slate'
import { Editor as EditorComponent } from 'slate-react'
import { Line } from './helpers/helper'
import { init, reducer } from './helpers/reducer'
import { BlockType } from './helpers/decorate/document/helper'
import lineParser from './helpers/decorate/line'
import renderMark from './helpers/render/mark'
import renderNode from './helpers/render/node'
import { onKeyDown } from './helpers/events'

const isLine = (node: Node): node is Line =>
  node.object === 'block' && node.type === BlockType.Line

const decorateNode =
  (node: Node, editor: Editor, next: () => Decoration[]): Decoration[] =>
    (next() || []).concat(isLine(node) ? lineParser(node) : [])

export default function EditArea () {
  // NOTE: can't useReducer as the value is mutated by the Editor methods
  const [value, setValue] = useState(init())

  return <EditorComponent
    className='slate-editor'
    value={value}
    onChange={changes => setValue(reducer(changes))}
    // @ts-ignore
    renderMark={renderMark}
    renderNode={renderNode}
    decorateNode={decorateNode}
    onKeyDown={onKeyDown}
  />
}
