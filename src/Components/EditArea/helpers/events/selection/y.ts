import { Editor, Range as SRange, Text } from 'slate'
import { findDOMNode, findRange } from 'slate-react'
import { isText, range, rangeFromPoint } from '../../utils/dom'
import { clamp } from '../../utils/number'
import { Maybe } from '../../utils/types'

export { nextYPos, rangeFromYPos }

interface YSeg { top: number, bottom: number }
interface YPos { elem: Element, rect: DOMRect, rects: DOMRect[], dX: number }

// TEMP: fix error in @types/slate-react@0.20.1
const toSRange = findRange as unknown as (r: Range, e: Editor) => SRange

const QUERY = '[data-slate-content],[data-slate-zero-width]'
const isAbove = (a: YSeg, b: YSeg) => a.bottom < b.top
const isUnder = (a: YSeg, b: YSeg) => a.top > b.bottom
const topAboveBottom = (a: YSeg, b: YSeg) => a.top < b.bottom
const bottomUnderTop = (a: YSeg, b: YSeg) => a.bottom > b.top
const hasHigherBottom = (a: YSeg, b: YSeg) => a.bottom < b.bottom
const hasLowerTop = (a: YSeg, b: YSeg) => a.top > b.top

function nextYPos
(cursor: DOMRect, mvForwd: boolean): (text: Text) => Maybe<YPos> {
  const reduceFn = mvForwd ? 'reduce' : 'reduceRight'
  const [fullyBefore, startsBeforeEnds, endsBefore]
    = mvForwd
      ? [isAbove, topAboveBottom, hasHigherBottom]
      : [isUnder, bottomUnderTop, hasLowerTop]

  return text => {
    const line = findDOMNode(text)
    const lineRect = line.getBoundingClientRect() as DOMRect
    if (!endsBefore(cursor, lineRect)) return
    const elems = line.querySelectorAll(QUERY) as NodeListOf<HTMLElement>

    return Array.from(elems)[reduceFn]((prev: Maybe<YPos>, elem) => {
      const y = lineRect.y + elem.offsetTop
      const seg = { top: y, bottom: y + elem.offsetHeight }
      return !endsBefore(cursor, seg) || nearest(seg, prev) === prev
        ? prev
        : findNearer(prev, elem)
    }, undefined)
  }

  function findNearer (init: Maybe<YPos>, elem: HTMLElement): Maybe<YPos> {
    const rects = Array.from(elem.getClientRects() as DOMRectList)
    return rects[reduceFn]((prev: Maybe<YPos>, rect) => {
      return startsBeforeEnds(rect, cursor)
        ? prev
        : nearest(rect, prev, next)

      function next (): YPos {
        const dL = rect.left - cursor.x
        const dR = rect.right - cursor.x
        const dX = +(dL * dR > 0) && Math.min(Math.abs(dL), Math.abs(dR))
        return { dX, rect, rects, elem }
      }
    }, init)
  }

  function nearest<T extends { dX: number }>
  (seg: YSeg, prev: Maybe<YPos>, fn = () => ({ dX: 0 }) as T): YPos | T {
    if (!prev) return fn()
    if (fullyBefore(prev.rect, seg)) return prev
    const res = fn()
    if (fullyBefore(seg, prev.rect)) return res
    return res.dX < prev.dX && startsBeforeEnds(seg, prev.rect) ? res : prev
  }
}

function rangeFromYPos (editor: Editor, cursor: DOMRect, pos: YPos): SRange {
  const text = pos.elem.firstChild
  if (!isText(text)) throw new Error('The picked node isn\'t Text')
  const x = clamp(cursor.x, pos.rect.x, pos.rect.right)
  const [length, offset] = pos.rects.reduce(amount, [0, 0])
  const rg = length > 0
    ? rangeFromPoint(text, x, pos.rect.y, offset / length)
    : range(text, 0)

  return toSRange(rg, editor)

  function amount ([l0, offset]: number[], rect: DOMRect) {
    return [l0 + rect.width, rect === pos.rect ? l0 + x - rect.x : offset]
  }
}
