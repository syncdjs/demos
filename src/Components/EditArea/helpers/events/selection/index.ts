import { List } from 'immutable'
import { Editor, Selection, Text, Value } from 'slate'
import { findDOMRange } from 'slate-react'
import { xor } from '../../utils/logic'
import { Maybe } from '../../utils/types'
import { nextYPos, rangeFromYPos } from './y'

export { yStep }

// TEMP: fix error in @types/slate-react@0.20.1
const toDOMRange = findDOMRange as unknown as (r: Selection) => Range

function yStep (evt: KeyboardEvent, editor: Editor, mvForwd: boolean): void {
  const mvFocus = evt.shiftKey
  const mvEnd = xor(mvFocus, editor.value.selection.isBackward)
  const cursor = getCursor(editor.value.selection, mvEnd)
  const textToYPos = nextYPos(cursor, mvForwd)

  const current = editor.value[mvFocus ? 'focusText' : 'anchorText']
  if (!current) throw new Error('No selected text element')

  const next = nextText(editor.value, mvForwd, mvEnd)
  const yPos = textToYPos(current) || next && textToYPos(next)

  if (yPos) {
    const rg = rangeFromYPos(editor, cursor, yPos)
    if (mvFocus) {
      const { offset, path } = rg.anchor
      editor.moveFocusTo(path as List<number>, offset) // MUTATION
    } else {
      editor.select(rg) // MUTATION
    }
  } else {
    const key = next ? next.key : current.key
    const offset = xor(mvForwd, !next) ? 0 : current.text.length
    editor[mvFocus ? 'moveFocusTo' : 'moveTo'](key, offset) // MUTATION
  }

  evt.preventDefault()
}

function getCursor (selection: Selection, mvEnd: boolean): DOMRect {
  const rects = toDOMRange(selection).getClientRects() as DOMRectList
  if (selection.isCollapsed) return rects[0]

  const { y, height, left, right } = rects[mvEnd ? rects.length - 1 : 0]
  return DOMRect.fromRect({ x: mvEnd ? right : left, y, height, width: 0 })
}

function nextText (v: Value, mvForwd: boolean, mvEnd: boolean): Maybe<Text> {
  if (mvForwd) {
    return mvEnd || v.texts.size === 1
      ? v.nextText
      : v.texts.get(1)
  } else {
    return !mvEnd || v.texts.size === 1
      ? v.previousText
      : v.texts.get(v.texts.size - 2)
  }
}
