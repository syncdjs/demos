import { Editor } from 'slate'
import { EventHook } from 'slate-react'
import { Key, KeyboardEvent } from '../utils/event'
import { yStep } from './selection'

export const onKeyDown = keyDown as unknown as EventHook

function keyDown (evt: KeyboardEvent, editor: Editor, next: () => void): void {
  switch (evt.key) {
    case Key.ArrowUp:
      return yStep(evt, editor, false)

    case Key.ArrowDown:
      return yStep(evt, editor, true)

    default:
      next()
  }
}
