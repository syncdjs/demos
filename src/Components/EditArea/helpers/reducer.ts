import { List, Map } from 'immutable'
import { Block, Node, Operation, Value } from 'slate'
import { unzipWith } from './utils/noncurried'
import { parse } from './utils/string'
import parser from './decorate/document/parser'
import initalValue from '../initial-value'

interface Changes { operations: List<Operation>, value: Value }
interface Root extends Block { nodes: List<Block> }

const rootPath = ['document', 'nodes', 0]
const editActions =
  [ 'insert_text', 'remove_text', 'split_node', 'merge_node', 'remove_node'
  , 'set_node', 'move_node', 'insert_node', 'set_value' ]

export const init = () => getValue(Value.fromJSON(initalValue))

export function reducer ({ operations, value }: Changes): Value {
  const opTypes = operations.toArray().map(op => op.type)
  const select = opTypes.includes('set_selection')
  const edit = opTypes.some(action => editActions.includes(action))
  return getValue(value, edit, select)
}

function getValue (value: Value, edit = true, select = false): Value {
  if (!select && !edit) return value

  const root0: Root = value.getIn(rootPath)
  const lines0 = root0.nodes.toArray()
  const lines1 = select ? setSel(lines0, value) : lines0
  const root1 = edit ? parseDoc(root0, lines1) : setNodes(root0, lines1)
  return value.setIn(rootPath, root1) as Value
}

function setSel (lines: Block[], value: Value): Block[] {
  const kSel = value.blocks.toArray().map(l => l.key)
  const tSel = value.selection
  return lines.map(l =>
    setData(l, 'selection', kSel.includes(l.key) ? tSel : undefined),
  )
}

function parseDoc (root: Root, lines0: Block[]): Root {
  const [keys, strs] = unzipWith(lines0, b => [b.key, b.text])
  const init = { items: [], open: [], n: 0, content: {} }
  const { items, content } = parse(init, strs.join('\n'), parser(keys))
  const lines1 = lines0.map(l => setData(l, 'content', content[l.key] || ''))
  return setData(setNodes(root, lines1), 'items', items)
}

function setNodes<P extends Block> (p: P, nodes: Node[]): P {
  return p.set('nodes', List(nodes)) as P
}

function setData<T extends Map<string, any>> (m: T, name: string, x: any): T {
  return m.setIn(['data', name], x) as T
}
