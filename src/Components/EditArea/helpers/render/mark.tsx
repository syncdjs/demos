
import * as React from 'react'
import { Mark as M, Selection } from 'slate'
import { RenderAttributes, RenderMarkProps } from 'slate-react'
import { join, matches } from '../utils/string'
import { Unreachable } from '../utils/types'
import { MarkType as MT } from '../decorate/line/helper'
import { addClass, mergeAttrs, parseAttrs } from './helper'

interface Mark extends M { type: MT }
interface Props extends RenderMarkProps { mark: Mark }

const ns = 'slate-mark'

export default renderMark

function renderMark (props: Props) {
  const { children, mark, attributes } = props
  const { data, type } = mark

  if (type === MT.Symb) {
    return <span className={type} {...attributes}>{children}</span>
  }

  const attrs0 = addClass(attributes, [
    ns,
    `${ns}-${type}`,
    !!data.get('selected') && `${ns}-selected`,
  ])
  const childAttrs = parseAttrs(data.get('attrs'), {})
  const attrs1 = () => mergeAttrs(attrs0, childAttrs)
  const attrs2 = () => addClass(attrs0, ['slate-void'])

  switch (type) {
    case MT.Strong:
      return <strong {...attrs1()}>{children}</strong>

    case MT.Emphasis:
      return <em {...attrs1()}>{children}</em>

    case MT.Link: {
      const [, title] = mdAttrs(data.get('md'))
      return <a href='#' {...{ title }} {...attrs1()}>{children}</a>
    }

    case MT.Tag:
      return React.createElement(data.get('tag') || 'span', attrs1(), children)

    case MT.Image: {
      const [src, title] = mdAttrs(data.get('md'))
      return <span {...attrs2()}>
        <img alt={data.get('alt')} {...{ src, title }} {...childAttrs} />
        <span className='symb'>{children}</span>
      </span>
    }

    case MT.EmptyTag:
      return <span {...attrs2()}>
        {React.createElement(data.get('tag') || 'span', childAttrs)}
        <span {...attributes} className='symb'>{children}</span>
      </span>

    case MT.Input:
      return <span {...attrs2()}>
        <input {...fieldsAttrs<'input'>(data.get('value'), childAttrs) }/>
        <span {...attributes} className='symb'>{children}</span>
      </span>

    case MT.TextArea: {
      const taAttrs = fieldsAttrs<'textarea'>(data.get('value'), childAttrs)
      const v = taAttrs.value || taAttrs.defaultValue
      const fAttrs = !v ? taAttrs : { ...taAttrs,
        placeholder: String(v),
        value: undefined,
        defaultValue: undefined,
        className: `${ns}-${type}--pholder-hack`,
      }
      return <span {...attrs2()}>
        <textarea {...fAttrs} />
        <span className='symb'>{children}</span>
      </span>
    }

    default:
      throw new Unreachable(type)
  }
}

function mdAttrs (str: string): string[] {
  return matches(str, /"(.*?)"|(?:^|\s+)(.*?)(?:$|\s+)/g).map(([, s]) => s)
}

function fieldsAttrs<T extends keyof JSX.IntrinsicElements>
(value?: string, attrs: RenderAttributes = {}): (JSX.IntrinsicElements)[T] {
  return { value, onChange: () => null, ...attrs }
}
