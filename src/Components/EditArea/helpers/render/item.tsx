import React, { ReactNode, createElement } from 'react'
import { BlockType as BT, ItemData } from '../decorate/document/helper'
import { Unreachable } from '../utils/types'
import { RenderData, addClass, parseAttrs } from './helper'

const ns = 'slate-node'

interface X extends RenderData, ItemData {}
export default function (x: X, children: ReactNode) {
  const attrs0 = addClass({ key: x.key }, [
    ns,
    `${ns}-${x.type}`,
    !!x.selected && 'slate-selected',
    !x.content && `${ns}-contentless`,
  ])

  switch (x.type) {
    case BT.Line:
      return <span {...attrs0} data-key={x.key}>{children}</span>

    case BT.Blank:
      return <div {...attrs0}>{children}</div>
  }

  const attrs1 = parseAttrs(x.attrs, attrs0)

  switch (x.type) {
    case BT.Paragraph:
      return <p {...attrs1}>{children}</p>

    case BT.Tag: {
      const tag = x.tag || 'div'
      const type = /*tag in components ? components[tag] :*/ tag
      return createElement(type, attrs1, children)
    }

    case BT.ListItem:
      return <li {...attrs1}>{children}</li>

    case BT.BulletList:
      return <ul {...attrs1}>{children}</ul>

    case BT.OrderedList:
      return <ol {...attrs1}>{children}</ol>

    case BT.Heading1:
      return <h1 {...attrs1}>{children}</h1>
    case BT.Heading2:
      return <h2 {...attrs1}>{children}</h2>
    case BT.Heading3:
      return <h3 {...attrs1}>{children}</h3>
    case BT.Heading4:
      return <h4 {...attrs1}>{children}</h4>
    case BT.Heading5:
      return <h5 {...attrs1}>{children}</h5>
    case BT.Heading6:
      return <h6 {...attrs1}>{children}</h6>

    default:
      throw new Unreachable(x.type)
  }
}
