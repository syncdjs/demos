import { Fragment, ReactChild, createElement } from 'react'
import { Editor } from 'slate'
import { RenderNodeProps as Props } from 'slate-react'
import { props } from '../utils/dict'
import { countRight } from '../utils/noncurried'
import { BlockType as BT, Item } from '../decorate/document/helper'
import { RenderData as Data } from './helper'
import renderItem from './item'

const linePath = ['document', 'nodes', 0, 'nodes']

export default function (props: Props, editor: Editor) {
  const { node, children } = props
  switch (node.type) {
    case BT.Line:
      return renderItem({ ...toData(node), type: BT.Line }, children)

    case 'root':
      const items: Item[] = node.data.get('items') || []
      const data = editor.value.getIn(linePath).toArray().map(toData)
      const x = items.reduceRight(renderRoot, { data, children } as X)
      return createElement(Fragment, {}, x.children)

    default:
      throw new Error(`Unknown block type: ${node.type}`)
  }
}

interface X { data: Data[], children: ReactChild[] }
export function renderRoot (x: X, item: Item, idx: number, items: Item[]): X {
  const [start, end] = props<string, string>([0, -1], item.keys)

  const first = x.data.findIndex(d => d.key === start)
  if (first === -1) return x

  const remain = x.data.slice(first)
  const last = first + remain.findIndex(d => d.key === end) + 1

  const key = `${start}-${countRight(items, i => i.keys[0] === start, idx - 1)}`
  const cData = x.data.slice(first, last)
  const content = cData.some(d => !!d.content)
  const selected = cData.some(d => d.selected)

  const children = x.children.slice(first, last)
  const child = renderItem({ ...item, key, content, selected }, children)

  return {
    data: insert(x.data, { key: start, content, selected }, first, last),
    children: insert(x.children, child, first, last),
  }
}

function insert <T> (lst: T[], i: T, n0: number, nk = n0 + 1) {
  return [...lst.slice(0, n0), i, ...lst.slice(nk)]
}

function toData (l: Props['node']): Data {
  const { selection, ...data } = l.data.toJS()
  return { ...data, key: l.key, selected: !!selection }
}
