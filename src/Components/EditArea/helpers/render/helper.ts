
import { RenderAttributes as Attrs } from 'slate-react'
import { chain, when, filter } from '../utils/noncurried'
import { matches, join } from '../utils/string'
import { Maybe } from '../utils/types'

export interface RenderData {
  key: string, selected: boolean, content: boolean | string }

export function parseAttrs (str: Maybe<string>, others: Attrs = {}): Attrs {
  const re = /^\s*(#[\w\-]+)?((?:\.\w[^.\s]*)+)?\s*(.+)?/
  return str && when(str.match(re), ([, idStr, clsStr, attrsStr]) => {
    const { className: cls0, ...attrs0 } = others
    const cls1 = clsStr && chain(clsStr.slice(1).split('.'), getCls).join(' ')
    const { class: cls2, className: cls3, ...attrs1 } = otherAttrs(attrsStr)
    const id = idStr && idStr.slice(1)
    const className = filter([cls0, cls1, cls2, cls3]).join(' ')
    return { ...attrs0, ...attrs1, id, className }
  }) || others
}

function getCls (cls: string): string[] {
  const re = /^([A-Za-z][\w\-]*?)([-_]*):(.*)$/
  return cls && (when(cls.match(re), parseClass) || [cls]) || []
}

function parseClass ([, x, link, ys]: RegExpMatchArray): string[] {
  return ys.split(':').map(y => y ? `${x}${link || '-'}${y}` : x)
}

function otherAttrs (str: Maybe<string>) {
  const re = /([A-Za-z][\w\-]+)(?:="(.*?)")?(?:\s+|$)/g
  return str && when(matches(str, re), (attrs): Attrs =>
    Object.assign({}, ...attrs.map(([, k, v = true]) => ({ [k]: v }))),
  ) || {}
}

type ClassName = string | false
export function addClass<T extends Attrs> (attrs: T, classes: ClassName[]): T {
  return { ...attrs, className: join([attrs.className, ...classes]) }
}

export function mergeAttrs<T extends Attrs>
({ className: c0, ...attrs0 }: T, { className: c1, ...attrs1 }: T): Attrs {
  return { ...attrs0, ...attrs1, className: join([c0, c1]) }
}
