import { Block } from 'slate'
import { BlockType } from './decorate/document/helper'

export interface Line extends Block { type: BlockType.Line }
