import { defaultFilter, filter as LstFilter, when } from './noncurried'
import { Just, Maybe } from './types'

export interface Dict<T> { [k: string]: T }
export type Obj<T extends Pick<T, keyof U>, U = T> = Pick<T, keyof T>

export function combine<T> (keys: (keyof T)[]): (vals: (T[keyof T])[]) => T {
  return vals => {
    const end = Math.min(keys.length, vals.length)
    let pairs = {} as T
    for (let i = 0 ; i < end ; i++) pairs[keys[i]] = vals[i]
    return pairs
  }
}

export function merge<T extends Pick<T, keyof T>> (lst: T[]): T
export function merge<T, U = Dict<T>> (lst: Dict<Maybe<T>>[]): U {
  return filter(Object.assign({}, ...LstFilter(lst)))
}

// export function dictSize<T> (dict: Maybe<Dict<T>>) {
//   return dict ? Object.keys(dict).length : 0
// }

interface IterFn<T extends Obj<T>, R> {
  (x: Just<T[keyof T]>, k: keyof T, d: T): R
}

export function map<T extends Obj<T>, R extends Obj<R, T> = T>
(obj: T, fn: IterFn<T, R[keyof T]>): R {
  const keys = Object.keys(obj) as (keyof T)[]
  const pairs = keys.map(k => when(obj[k], v => ({ [k]: fn(v, k, obj) }) || {}))
  return Object.assign({}, ...pairs)
}

export function filter<T extends Obj<T>, R extends Obj<R, T> = T>
(obj: T, fn: IterFn<T, boolean> = defaultFilter): R {
  const keys = Object.keys(obj) as (keyof T)[]
  const pairs = keys.map(k => fn(obj[k], k, obj) ? { [k]: obj[k] } : {})
  return Object.assign({}, ...pairs)
}

// export function map<T, U> (dict: Dict<T>, fn: IterFn<T, U>): Dict<U> {
//   return merge(
//     Object.keys(dict)
//       .map(k => when(fn(dict[k], k, dict), x => ({ [k]: x })) || {}),
//   )
// }

type ExtDict<T> = Dict<T> | Array<T>
type ExtKey = string | number

function isDict<T> (o: any): o is Dict<T> {
  return !!o && typeof o === 'object'
}

function prop<T, R = Maybe<T>, D = ExtDict<T>>
  (k: ExtKey, o: Maybe<D>): R
function prop<T extends Pick<T, K>, K extends keyof T = keyof T>
  (key: K, dict: T): T[K]
function prop<T> (k: ExtKey, o: Maybe<ExtDict<T>>) {
  if (Array.isArray(o)) {
    const n = Number(k)
    return n >= 0 ? o[n] : o[o.length - 1 + (n + 1) % o.length]
  } else if (isDict<T>(o)) {
    return o[k]
  }
}

export function props<T, R extends Maybe<T> = Maybe<T>, D = ExtDict<T>>
  (keys: ExtKey[], dict: D): R[]
export function props<T extends Pick<T, K>, K extends keyof T = keyof T>
  (keys: K[], dict: T): T[K][]
export function props<T> (keys: ExtKey[], dict: ExtDict<T>) {
  return keys.map(k => prop(k, dict))
}

export function pick <X extends Partial<T>, Y, T extends Y & Partial<X>>
(ks: (keyof T)[], x: X, y: Y = {} as Y): T {
  return Object.assign({}, y, ...ks.map(k => k in x ? { [k]: x[k] } : {}))
}

interface NestedArr<T> extends Array<Nested<T>> {}
interface NestedDict<T> { [k: string]: Nested<T> }
type Nested<T> = NestedDict<T> | NestedArr<T> | Maybe<T>
export function get<T> (path: string, obj: Maybe<Nested<T>>): Maybe<T> {
  return path.split('.')
    .reduce((o, k) => prop<T, Nested<T>, Nested<T>>(k, o), obj) as Maybe<T>
}
