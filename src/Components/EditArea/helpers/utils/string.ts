import { Maybe, Tuple } from './types'
import { filter } from './noncurried'

export function descape (str: string): string {
  return str.replace(/\\(.)/g, '$1')
}

interface ParseUpdate<T> extends Tuple { 0: number, 1: T }
type Parser<T> = (state: T, s: string, i: number) => Maybe<ParseUpdate<T>>
const mvErrMsg = 'The increment must be > 0, but got:'
export function parse<T> (state: T, text: string, fn: Parser<T>): T {
  const ini: [string, number, number] = [text, 0, 0]
  for (let [str, idx, incr] = ini ; str.length > 0 ; str = str.slice(incr)) {
    [incr, state] = fn(state, str, idx) || [1, state] // !!! state is reasigned
    if (incr <= 0) throw new Error(`${mvErrMsg} ${incr}`)
    idx += incr
  }
  return state
}

export function matches (str: string, re: RegExp): RegExpExecArray[] {
  let matches = []
  // tslint:disable-next-line no-conditional-assignment
  for (let match ; match = safeMatch(str, re) ;) {
    const filtered = filter(match) as RegExpExecArray
    filtered.index = match.index
    filtered.input = match.input
    matches.push(filtered)
  }
  return matches
}

function safeMatch (str: string, re: RegExp): Maybe<RegExpExecArray> {
  if (re.lastIndex < str.length) {
    const match = re.exec(str)
    if (match && match.input) return match
  }
}

export function join (classes: Maybe<string>[], glue = ' '): string {
  return classes.filter(x => !!x).join(glue)
}
