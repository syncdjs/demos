export interface Dict<T> { [k: string]: T }

export type Maybe<T> = T | undefined
export type Just<T> = T extends undefined ? never : T

export function isdef<T> (x: Maybe<T>): x is T {
  return typeof x !== 'undefined'
}

export interface Tuple<T = any> extends Array<T> {}

export interface ImmutableReducer<R, V> { (reduction?: R, value?: V): R }

export class Unreachable extends Error {
  constructor (val: never) { super(`Unreachable case: ${val}`) }
}
