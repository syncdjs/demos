export function isText (n: Node | null): n is Text {
  return !!n && n.nodeName === '#text'
}

export function range (node: Node, offset: number) {
  const rg = document.createRange()
  rg.setStart(node, offset)
  return rg
}

/**
 * NOTES on rangeFromPoint:
 * 1. Will only work with a **left to right** text node.
 * 2. The y coordinate must be exact.
 */
export function rangeFromPoint (t: Text, x: number, y: number, r = .5): Range {
  {
    const offset = Math.round(r * t.length)
    const rg = range(t, offset)
    const rect = rg.getBoundingClientRect() as DOMRect
    const dY = y - rect.y

    if (dY === 0) {
      const dX = x - rect.x
      const step = dX < 0 ? -1 : 1
      if (dX === 0 || overflow(offset, step)) return rg
      return closer(rg, offset, step, dX)
    } else {
      const step = dY < 0 ? -1 : 1
      if (overflow(offset, step)) return rg
      return closer(rg, offset, step)
    }
  }

  function closer (rg0: Range, offset0: number, step: -1 | 1, d0 = 0): Range {
    const offset1 = offset0 + step
    const rg1 = range(t, offset1)
    const rect = rg1.getBoundingClientRect() as DOMRect
    const d1 = x - rect.x

    if (d0) {
      if (Math.abs(d1) >= Math.abs(d0) || rect.y !== y) return rg0
      if (d0 * d1 <= 0 || overflow(offset1, step)) return rg1
    } else {
      if (overflow(offset1, step)) return rg1
      if (rect.y !== y) return closer(rg1, offset1, step)
      if (step * d1 <= 0) return rg1
    }
    return closer(rg1, offset1, step, d1)
  }

  function overflow (offset: number, step: -1 | 1) {
    return offset === (step === -1 ? 0 : t.length)
  }
}
