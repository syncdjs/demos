export function xor (a: any, b: any): boolean {
  return a && !b || !a && b
}
