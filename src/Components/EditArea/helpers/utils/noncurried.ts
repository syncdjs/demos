import { List } from 'immutable'
import { Maybe } from './types'

// Fix typing issues with immutable.List
type UIterable<T> = List<T> | Iterable<T>
type UList<T> = Array<T> | List<T>

export function unzipWith<T, U, V> (l: UList<Maybe<T>>, fn: (x: T) => [U, V]) {
  return (l as Maybe<T>[]).reduce(
    ([us, vs], x): [U[], V[]] => when(x, (x) => {
      const [u, v] = fn(x)
      return [[...us, u], [...vs, v]] as [U[], V[]]
    }) || [us, vs],
    [[], []] as [U[], V[]],
  )
}

export function adjust<T> (lst: T[], idx: Maybe<number>, fn: (x: T) => T): T[] {
  return typeof idx === 'number' && idx in lst
    ? [...lst.slice(0, idx), fn(lst[idx]), ...lst.slice(idx + 1)]
    : lst
}

export function update<T> (lst: T[], idx: Maybe<number>, value: T): T[] {
  return typeof idx === 'number' && idx in lst
    ? [...lst.slice(0, idx), value, ...lst.slice(idx + 1)] as T[]
    : [...lst, value] as T[]
}

export function remove<T> (lst: T[], idx: Maybe<number>): T[] {
  return typeof idx === 'number' && idx in lst
    ? [...lst.slice(0, idx), ...lst.slice(idx + 1)]
    : lst
}

// export function ifElse<T> (x: any, tFn: () => T, fFn?: () => T): Maybe<T> {
//   return x ? tFn() : fFn ? fFn() : undefined
// }

export function when<T, U> (x: Maybe<T> | null, fn: (x: T) => U): Maybe<U> {
  return typeof x === 'undefined' || x === null ? undefined : fn(x)
}

export interface ReducerFn <Acc, T> {
  (acc: Acc, x: T, i: number, l: Iterable<T>): Acc
}
export const reduce =
  Array.prototype.reduce.call.bind(Array.prototype.reduce) as
  <Acc, T>(l: Iterable<T>, fn: ReducerFn<Acc, T>, init: Acc) => Acc

interface IterFn<T, U> { (x: T, i: number, l: Iterable<T>): U }

export const map =
  Array.prototype.map.call.bind(Array.prototype.map) as
  <T, U>(l: Iterable<T>, fn: IterFn<T, U>) => U[]

export const every =
  Array.prototype.every.call.bind(Array.prototype.every) as
  <T>(l: Iterable<T>, fn: IterFn<T, boolean>) => boolean

export function defaultFilter<T> (x: T): boolean {
  return typeof x !== 'undefined'
}

export function filter<T> (lst: Iterable<Maybe<T>>): T[]
export function filter<T>
(lst: Iterable<T>, fn: IterFn<T, boolean> = defaultFilter): T[] {
  return Array.prototype.filter.call(lst, fn)
}

type FromFn<T, U> = IterFn<T, Maybe<U> | null>

export function fromIterable<T, U>
(list: UIterable<T>, fn: FromFn<T, U>): Maybe<U> {
  let index = 0
  for (const item of list as Iterable<T>) {
    const res = fn(item, index++, list as Iterable<T>)
    if (typeof res !== 'undefined' && res !== null) return res
  }
}

type Itr = string | any[]

export function fromList<U>
  (itr: string, fn: FromFn<string, U>, i0?: number): Maybe<U>
export function fromList<T, U>
  (itr: T[], fn: FromFn<T, U>, i0?: number): Maybe<U>
export function fromList<U> (itr: Itr, fn: FromFn<any, U>, i0 = 0) {
  for (let index = i0 ; index < itr.length ;) {
    const res = fn(itr[index], index++, itr)
    if (typeof res !== 'undefined' && res !== null) return res
  }
}
export function lastFromList<U>
  (itr: string, fn: FromFn<string, U>, i0?: number): Maybe<U>
export function lastFromList<T, U>
  (itr: T[], fn: FromFn<T, U>, i0?: number): Maybe<U>
export function lastFromList<U> (itr: Itr, fn: FromFn<any, U>) {
  for (let index = itr.length - 1 ; index >= 0 ;) {
    const res = fn(itr[index], index--, itr)
    if (typeof res !== 'undefined' && res !== null) return res
  }
}

// export function fromWholeList<T, U> (itr: T[], fn: FromFn<T, U>): Maybe<U[]> {
//   let resLst = []
//   for (let index = 0 ; index < itr.length ;) {
//     const res = fn(itr[index], index++, itr)
//     if (typeof res === 'undefined' || res === null) return undefined
//     resLst.push(res)
//   }
//   return resLst
// }

export function count
(str: string, fn: IterFn<string, boolean>, i0?: number): number
export function count<T>
(lst: T[], fn: IterFn<T, boolean>, i0?: number): number
export function count
(itr: Itr, fn: IterFn<any, boolean>, i0: number = 0) {
  let index = i0
  while (index < itr.length && fn(itr[index], index, itr)) index++
  return index - i0
}

export function countRight
  (str: string, fn: IterFn<string, boolean>, i0?: number): number
export function countRight<T>
  (lst: T[], fn: IterFn<T, boolean>, i0?: number): number
export function countRight
(itr: Itr, fn: IterFn<any, boolean>, i0: number = itr.length - 1) {
  let index = i0
  while (index >= 0 && fn(itr[index], index, itr)) index--
  return i0 - index
}

export function chain<T, U>
(list: Iterable<T>, fn: IterFn<T, Maybe<Iterable<U>>>): U[] {
  let index = 0
  let res = []
  for (const item of list) res.push(...(fn(item, index++, list) || []))
  return res
}

// export function concat<T> (...lst: Maybe<T[]>[]): T[] {
//   return ([] as T[]).concat(...filter(lst))
// }

export function split<T> (lst: T[], n: number): [T[], T[]] {
  const i = n >= 0 ? n : lst.length + (n + 1) % lst.length
  return [lst.slice(0, i), lst.slice(i)]
}
