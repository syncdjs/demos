const { max, min } = Math

export function clamp (n: number, lower: number, higher: number) {
  return max(lower, min(higher, n))
}
