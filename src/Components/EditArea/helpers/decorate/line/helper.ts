import { Maybe, Tuple } from '../../utils/types'

export enum MarkType {
  Symb = 'symb',
  Emphasis = 'emphasis',
  Strong = 'strong',
  Link = 'link',
  Image = 'image',
  Input = 'input',
  TextArea = 'textarea',
  Tag = 'tag',
  EmptyTag = 'empty-tag',
}

export interface Data {
  md?: string, attrs?: string, tag?: string, alt?: string, value?: string,
}
export interface Vect extends Tuple { 0: number, 1: number }

interface Match extends Data { s0: string, length?: number, type?: MarkType }
export interface ClsMatch extends Match { type: MarkType }
export interface OpMatch extends Match { patterns: Pattern[] }
export interface Pattern { (s: string): Maybe<ClsMatch | OpMatch> }
