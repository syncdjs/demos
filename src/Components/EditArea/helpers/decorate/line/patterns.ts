import { when } from '../../utils/noncurried'
import { Maybe } from '../../utils/types'
import { toMatch as to, voidTags } from '../helper'
import { MarkType as MT, ClsMatch, OpMatch, Pattern } from './helper'

interface ClsMatchFn { (s: string[]): Maybe<ClsMatch> }
interface OpMatchFn { (s: string[]): Maybe<OpMatch> }
interface MatchFn { (s: string[]): ReturnType<Pattern> }

const toMatch = to<ClsMatch>('s0')
const enclose = (...p: Pattern[]): OpMatchFn => ([s0]) => ({ s0, patterns: p })

const matchLink = toMatch({ type: MT.Link }, 'md', 'attrs')
const matchImg = toMatch({ type: MT.Image }, 'alt', 'md', 'attrs')

const notVoid: ClsMatchFn = ([s0, tag, attrs]) => {
  if (!voidTags[tag]) return { s0, attrs, tag, type: MT.Tag }
}

const encloseTag: OpMatchFn = ([s0, tag, attrs]) => {
  const data = { tag, attrs, type: MT.Tag }
  const patterns = !voidTags[tag]
    ? [m(RegExp(`^< */ *${tag || ''} *>`), toMatch(data))]
    : []

  return { s0, ...data, type: MT.EmptyTag, patterns }
}

const fieldRe = /^(?:\[([^\[\]]*)\])?< *(input|textarea)([#. ].*?)? *?>/
const matchField: ClsMatchFn = ([s0, value, tag, attrs]) => {
  switch (tag) {
    case 'input': return { s0, value, tag, attrs, type: MT.Input }
    case 'textarea': return { s0, value, tag, attrs, type: MT.TextArea }
  }
}

const patterns: Pattern[] =
  [ m('\\', () => ({ type: MT.Symb, s0: '\\', length: 2 }))
  , m('**', enclose(mAttrs('\\*\\*(?:{})?', MT.Strong)))
  , m('__', enclose(mAttrs('__(?:{})?', MT.Strong)))
  , m('*', enclose(mAttrs('\\*(?:{})?', MT.Emphasis)))
  , m('_', enclose(mAttrs('_(?:{})?', MT.Emphasis)))
  , m(fieldRe, matchField)
  , mTag('{}', encloseTag)
  , mTag('\\[\\]{}', toMatch({ type: MT.EmptyTag }, 'tag', 'attrs'))
  , m('[', enclose(mMdAttrs('{}', matchLink), mTag('\\]{}', notVoid)))
  , mMdAttrs('\\!\\[(.*?){}', matchImg) ]

export default patterns

function m (nd: string | RegExp, fn: MatchFn): Pattern {
  return typeof nd === 'string'
    ? s => { if (s && s.startsWith(nd)) return fn([nd]) }
    : s => { if (s) return when(s.match(nd), r => { if (r[0]) return fn(r) }) }
}

function mAttrs (nd: string, x: MatchFn | MT): Pattern {
  const fn = typeof x === 'function' ? x : toMatch({ type: x }, 'attrs')
  return m(RegExp(`^${ nd.replace('{}', '< *([^</>].*?)?>') }`), fn)
}

function mMdAttrs (nd: string, fn: MatchFn): Pattern {
  return mAttrs(nd.replace('{}', '\\]\\((.*?)\\)(?:{})?'), fn)
}

function mTag (nd: string, fn: MatchFn): Pattern {
  const re = /^([\w\-]*) *(.*)/
  return mAttrs(nd, ([s0, fullTag]) =>
    fullTag
      ? m(re, ([, tag, attrs]) => fn([s0, tag, attrs]))(fullTag)
      : fn([s0]))
}
