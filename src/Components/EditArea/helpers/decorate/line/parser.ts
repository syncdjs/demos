import { map } from '../../utils/dict'
import { chain, fromList, remove, when } from '../../utils/noncurried'
import { descape, parse } from '../../utils/string'
import { Maybe, Tuple } from '../../utils/types'
import { Data, MarkType, Pattern, Vect } from './helper'
import pattList from './patterns'

interface AbsItem extends Data { vects: Vect[], type?: MarkType }
interface ClsItem extends AbsItem { type: MarkType }
interface OpItem extends AbsItem { patterns: Pattern[] }
type Item = ClsItem | OpItem

interface State extends Tuple { 0: ClsItem[], 1: OpItem[] }
interface Update extends Tuple { 0: number, 1: State }
type Result = [Vect[], MarkType, Data]

export default function (str: string): Result[] {
  const [cls, op] = parse([[], []], str, parser)
  return [...cls, ...chain(op, closeOpened)].map(toResult)
}

function closeOpened ({ type, patterns, ...data }: OpItem): ClsItem[] {
  return type ? [{ type, ...data }] : []
}
function toResult ({ vects, type, ...data }: ClsItem): Result {
  return [vects, type, map<Data>(data, descape)]
}

function parser ([cls0, op0]: State, str: string, idx: number): Maybe<Update> {
  return when(op0[0], fromOpItem) || fromList(pattList, fromPattern())

  function fromOpItem ({ vects, patterns }: OpItem, i = 0): Maybe<Update> {
    return when(fromList(patterns, fromPattern(vects)),
      ([len, [cls1, op1]]): Update => [len, [cls1, remove(op1, i)]],
    )
  }

  function fromPattern (vect: Vect[] = []): (p: Pattern) => Maybe<Update> {
    return pattern => when(pattern(str), ({ s0, length, ...match }): Update => {
      const item: Item = { ...match, vects: [...vect, [idx, s0.length]] }
      const state: State = 'patterns' in item
        ? [cls0, [item, ...op0 ]]
        : [[...cls0, item], op0]

      return [length || s0.length, state]
    })
  }
}
