import { Decoration, PointJSON, Selection } from 'slate'
import { chain, when } from '../../utils/noncurried'
import { Maybe } from '../../utils/types'
import { Line } from '../../helper'
import { length, point } from '../helper'
import { MarkType, Vect } from './helper'
import parse from './parser'

interface PtFn { (i: number): PointJSON }

export default parseLine

function parseLine (line: Line): Decoration[] {
  const { key } = line.getTexts().first()
  const pt = (start: number) => (len: number) => point(key, start + len)

  const content: Maybe<string> = line.data.get('content')
  const selected: Maybe<Selection> = line.data.get('selection')
  const offset = line.text.length - length([content])

  return [
    ...offset && fromSymb(offset, pt(0)) || [],
    ...content && fromContent(key, offset, content, selected) || [],
  ]
}

function fromSymb (offset: number, pt: PtFn): Decoration[] {
  const mark = { type: MarkType.Symb }
  return [Decoration.fromJSON({ anchor: pt(0), focus: pt(offset), mark })]
}

function fromContent
(key: string, offset: number, str: string, sel: Maybe<Selection>): Decoration[] {
  const pt = (x: number) => point(key, x)
  return chain(parse(str), ([vects, mType, ItemData]): Decoration[] => {
    const [v0, v1] = vects
    const getSel = when(sel, ({ start, end }) => (x0: number, x1: number) => {
      if (start.key !== key && end.key !== key) return true
      if (end.key !== key) return start.offset > x0
      if (start.key !== key) return end.offset <= x1
      return start.offset > x0 && end.offset <= x1
    })
    const dec = (type: MarkType) => ([x, l]: Vect): Decoration => {
      const x0 = offset + x
      const x1 = x0 + l
      const selected = getSel && getSel(x0, x1)
      const data = { ...ItemData, selected }
      const json = { anchor: pt(x0), focus: pt(x1), mark: { type, data } }
      return Decoration.fromJSON(json)
    }

    if (v1) {
      const start = v0[0] + v0[1]
      const mainDec = dec(mType)([start, v1[0] - start])
      return [...vects.map(dec(MarkType.Symb)), mainDec]
    } else {
      return [dec(mType)(v0)]
    }
  })
}
