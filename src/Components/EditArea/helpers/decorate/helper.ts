import { PointJSON } from 'slate'
import * as voidElements from 'void-elements'
import { combine } from '../utils/dict'
import { Maybe } from '../utils/types'

export const voidTags: typeof voidElements = { ...voidElements,
  menuitem: true,
  textarea: true,
}

export function last (str: string): Maybe<string>
export function last<T> (lst: T[]): Maybe<T>
export function last (lst: any[] | string) {
  return lst[lst.length - 1]
}

export function length (strs: Maybe<string>[]) {
  return strs.reduce((sum, str) => sum + (str ? str.length : 0), 0)
}

export function point (key: string, offset = 0): PointJSON {
  return { key, offset, object: 'point' }
}

type M = { type: any }
type Attrs<T extends M> = Partial<T> & { type: T['type'] }
type Fn<T extends M> = (s: T[(keyof T)][]) => T
export function toMatch<T extends M>
(...k0: (keyof T)[]): (attrs: Attrs<T>, ...k1: (keyof T)[]) => Fn<T> {
  return (obj, ...k1) => vs => ({ ...combine<T>([...k0, ...k1])(vs), ...obj })
}
