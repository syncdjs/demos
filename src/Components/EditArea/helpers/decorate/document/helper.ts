export enum BlockType {
  Line = 'line',
  Blank = 'blank',
  Tag = 'tag',
  ListItem = 'list-item',
  BulletList = 'bullet-list',
  OrderedList = 'ordered-list',
  Paragraph = 'paragraph',
  Heading1 = 'heading-1',
  Heading2 = 'heading-2',
  Heading3 = 'heading-3',
  Heading4 = 'heading-4',
  Heading5 = 'heading-5',
  Heading6 = 'heading-6',
}

interface Flags { child?: BlockType, decorate?: boolean }
export interface ItemData { type: BlockType, tag?: string, attrs?: string }
interface D extends Flags, ItemData {}
export interface Match extends D { s0: string, spc: string, content?: string }
export interface Item extends D { keys: string[], lvl: number }
