import { pick } from '../../utils/dict'
import { fromList, lastFromList, split, update, when } from '../../utils/noncurried'
import { Tuple, Maybe, isdef } from '../../utils/types'
import { last, length } from '../helper'
import { BlockType as BT, Item, Match } from './helper'
import patterns from './patterns'

interface Nodes { items: Item[], open: number[] }
interface State extends Nodes { n: number, content: { [k: string]: string } }
interface Update extends Tuple { 0: number, 1: State }

const INDENT_SIZE = 3

export default function (keys: string[]) {
  return (state: State, str: string): Update =>
    fromList(patterns, (pattern): Maybe<Update> =>
      when(pattern(str), (m): Update => [m.s0.length, updateState(state, m)]),
    ) || [0, state] // Shouldn't get there. TODO replace 0 by str.length

  function updateState ({ n, content, ...nodes }: State, match: Match): State {
    const newLine = match.s0[0] === '\n'
    const n0 = n + +newLine
    const ks = match.s0.slice(+newLine).split('\n').map((l, i) => keys[n0 + i])
    const c = match.content
    return {
      ...updateNodes({ ...nodes, ...match, ks, newLine }),
      n: n0 + ks.length - 1,
      content: c && when(last(ks), k => ({ ...content, [k]: c })) || content,
    }
  }
}

interface X extends Match, Nodes { ks: string[], newLine: boolean }
function updateNodes (x: X): Nodes {
  if (x.type === BT.Blank) {
    return { items: addBlank(x.items, x.ks, x.open[0]), open: [] }
  }

  if (x.newLine) {
    const leadingLines = x.spc.slice(1).split('\n')
    const lvl = Math.floor(length([last(leadingLines)]) / INDENT_SIZE)
    const open = lastFromList(x.open, (n, i) => {
      if (x.items[n].lvl <= lvl) return x.open.slice(0, i + 1)
    }) || []

    const [clsKs, opKs] = split(x.ks, leadingLines.length - 1)
    const items = addBlank(x.items, clsKs, last(open))
    const added = opKs[0] ? newItems(x, { keys: opKs, lvl }) : []

    const noAttrs = !isdef(x.attrs)
    const txtOnly = noAttrs && x.type === BT.Paragraph
    const canFuse = (noAttrs && !clsKs[0]) || opKs[1]
    const canBeDecorated = (txtOnly || opKs[1]) && !clsKs[0]

    return lastFromList(open, (n, i) => {
      const { decorate, ...p } = items[n]
      const isSbl = p.lvl === lvl
      const fuse = isSbl && canFuse && (txtOnly || p.type === x.type)
      const deco = isSbl && canBeDecorated && decorate && i === open.length - 1

      if (p.lvl < lvl || fuse || deco) {
        const invalidPType = !isSbl && p.type === BT.Paragraph
        const type = invalidPType ? BT.Tag : (deco ? x.type : p.type)
        const keys = p.keys.concat(added[0].keys)
        const nodes = {
          items: update(items, n, { ...p, type, keys }),
          open: open.slice(0, i + 1),
        }
        return addItems(nodes, added.slice(+isSbl))
      }
    }) || addItems({ items, open: [] }, added)
  } else {
    const lvl = when(last(x.open), n => x.items[n].lvl) || 0
    return addItems(x, newItems(x, { keys: x.ks, lvl }))
  }
}

interface Y { keys: string[], lvl: number }
function newItems (x: X, y: Y): Item[] {
  const item = pick<X, Y, Item>(['type', 'tag', 'attrs', 'decorate'], x, y)
  return x.child
    ? [{ ...item, keys: [y.keys[0]] }, { ...item, type: x.child }]
    : [item]
}

function addBlank (items: Item[], keys: string[], n: Maybe<number>): Item[] {
  if (!keys[0]) return items

  const item: Item = { type: BT.Blank, keys, lvl: 0 }
  if (!isdef(n)) return items.concat([item])

  const parent: Item = { ...items[n], keys: items[n].keys.concat([keys[0]]) }
  return update(items, n, parent).concat([item])
}

function addItems ({ items, open }: Nodes, added: Item[]): Nodes {
  return {
    items: items.concat(added),
    open: open.concat(added.map(({}, n) => items.length + n)),
  }
}
