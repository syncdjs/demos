import { when } from '../../utils/noncurried'
import { Maybe } from '../../utils/types'
import { toMatch as to, voidTags } from '../helper'
import { BlockType as BT, Match } from './helper'

interface Pattern { (s: string): Maybe<Match> }
interface MatchFn { (s: string[]): ReturnType<Pattern> }

const toMatch = to<Match>('s0', 'spc')

const matchTag: MatchFn = ([s0, spc, attrs, content]) => {
  const data = { s0, spc, content }
  const tagMatch = attrs.match(/^\s*([\w\-]+)([#.\s][\s\S]*|.{0})$/)
  if (tagMatch) {
    const [, tag, attrs1] = tagMatch
    if (!voidTags[tag]) return { ...data, type: BT.Tag, tag, attrs: attrs1 }
  } else return { ...data, type: BT.Paragraph, attrs, decorate: !content }
}

const matchList = (type: BT): MatchFn =>
  toMatch({ type, child: BT.ListItem }, 'attrs', 'content')

const matchHeading: MatchFn = ([s0, spc, hashes, attrs, content]) => {
  switch (hashes.length) {
    case 1: return ({ type: BT.Heading1, s0, spc, attrs, content })
    case 2: return ({ type: BT.Heading2, s0, spc, attrs, content })
    case 3: return ({ type: BT.Heading3, s0, spc, attrs, content })
    case 4: return ({ type: BT.Heading4, s0, spc, attrs, content })
    case 5: return ({ type: BT.Heading5, s0, spc, attrs, content })
    default: return ({ type: BT.Heading6, s0, spc, attrs, content })
  }
}

const tagRe = '<((?:[^<>]*)?)>'
const contentRe = '(?: {1,2}(\\S.*)| +|.{0}(?=\\n|$))'
const toRe = (s: string) => RegExp(`^(\\s*)${s}${contentRe}`)

const patterns: Pattern[] =
  [ m(/^((?:\n *){2,})(?=\n|$)/, toMatch({ type: BT.Blank }))
  , m(`-(?:${tagRe})?`, matchList(BT.BulletList))
  , m(`\\d+\\.(?:${tagRe})?`, matchList(BT.OrderedList))
  , m(`(#+)(?:${tagRe})?`, matchHeading)
  , m(tagRe, matchTag)
  , m(/^(\s*)(.*)/, toMatch({ type: BT.Paragraph }, 'content')) ]

export default patterns

function m (needle: string | RegExp, fn: MatchFn): Pattern {
  const re = typeof needle === 'string' ? toRe(needle) : needle
  return str => {
    if (str) return when(str.match(re), m => { if (m[0]) return fn(m) })
  }
}
