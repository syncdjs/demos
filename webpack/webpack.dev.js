const { config, serverRoot } = require("./webpack.common")("/dist/")

module.exports = {
  ...config,

  mode: "development",

  // Set debugging source maps to be "inline" for
  // simplicity and ease of use
  devtool: "inline-source-map",

  devServer: {
    contentBase: serverRoot(""),
    port: 9000, // For some reason websocket keeps on failing on port 8080 !?
    watchContentBase: true,
  }
}
