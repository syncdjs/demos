const path = require("path")

const serverRoot = p => path.resolve(__dirname, "../public", p)

module.exports = publicPath => ({ serverRoot, config: {
  // The application entry point
  entry: "./src/index.tsx",

  // Where to compile the bundle
  output: {
    clean: true,
    path: serverRoot("dist"),
    publicPath,
    filename: "bundle.js",
  },

  // Supported file loaders
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader"
      },
      {
        test: /\.s[ac]ss$/i,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
    ]
  },

  // File extensions to support resolving
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
}})
